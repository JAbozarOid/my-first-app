package com.daavatapps.abozar.toolbar;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowCardsDetailes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cards_detailes);



        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/isans.ttf");

        int imageResource = getIntent().getIntExtra("image_drawable_id",-1);
        ImageView imageView = (ImageView)findViewById(R.id.imgApplication);
        imageView.setImageResource(imageResource);

        Bundle getNameTitle = getIntent().getExtras();
        String showNameTitle = getNameTitle.getString("title_id");
        TextView tvTile = (TextView) findViewById(R.id.txtTitleApp);
        tvTile.setText(showNameTitle);
        tvTile.setTypeface(typeface);

        Bundle getNameDescription1 = getIntent().getExtras();
        String showNameDescription = getNameDescription1.getString("description1_id");
        TextView tvDescription1 = (TextView) findViewById(R.id.txtDescriptionApp1);
        tvDescription1.setText(showNameDescription);
        tvDescription1.setTypeface(typeface);
    }
}
