package com.daavatapps.abozar.toolbar.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.daavatapps.abozar.toolbar.ListData;
import com.daavatapps.abozar.toolbar.MainActivity;
import com.daavatapps.abozar.toolbar.R;
import com.daavatapps.abozar.toolbar.ShowCards;
import com.daavatapps.abozar.toolbar.ShowCardsDetailes;

import java.util.ArrayList;

/**
 * Created by Abozar on 11/26/2016.
 */

public class DavaatFeaturesAdapter extends BaseAdapter {
    Context context;
    ArrayList<ListData> myList = new ArrayList<ListData>();
    LayoutInflater inflater;

    public DavaatFeaturesAdapter(Context context, ArrayList<ListData> myList) {
        this.context = context;
        this.myList = myList;
        inflater= LayoutInflater.from(this.context);

    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        MyViewHolder mViewHolder;

        if(convertView==null){

            convertView=inflater.inflate(R.layout.activity_list_data,null);
            mViewHolder = new MyViewHolder();
            convertView.setTag(mViewHolder);

        } else {

            mViewHolder = (MyViewHolder) convertView.getTag();
        }


        mViewHolder.txtTitleApp = detail(convertView,R.id.txtTitleApp,myList.get(position).getTitle());
        mViewHolder.txtDescriptionApp1 = detail(convertView,R.id.txtDescriptionApp1,myList.get(position).getDescription1());
        mViewHolder.imgApplication = detail(convertView,R.id.imgApplication,myList.get(position).getImage());

        mViewHolder.imgApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parent.getContext(), ShowCardsDetailes.class);
                intent.putExtra("image_drawable_id",myList.get(position).getImage());
                intent.putExtra("title_id",myList.get(position).getTitle());
                intent.putExtra("description1_id",myList.get(position).getDescription1());
                context.startActivity(intent);
            }
        });




        return convertView;
    }

    private TextView detail(View v,int resId,String text){

        TextView tv = (TextView) v.findViewById(resId);
        Typeface font = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Vazir-Light.ttf");
        tv.setTypeface(font);
        tv.setText(text);
        return tv;

    }

    private ImageView detail(View v,int resId,int icon){

        ImageView iv = (ImageView) v.findViewById(resId);
        iv.setImageResource(icon);
        return iv;


    }

    private class MyViewHolder {
        TextView txtTitleApp,txtDescriptionApp1;
        ImageView imgApplication;
    }

    public void addNewItemToList(ListData newData) {
        myList.add(0,newData);
        notifyDataSetChanged();

    }



}
